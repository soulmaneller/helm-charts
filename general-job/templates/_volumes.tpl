{{ define "volumes" }}
{{- if .Values.persistence.enabled }}
  {{- range .Values.persistence.pvcs }}
- name: vol-{{ .name }}
  persistentVolumeClaim:
    claimName: {{ .name }}
  {{- end }}
{{- end }}
{{- range .Values.configmap }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-config-{{ .name }}
  configMap:
    name: {{ .name }}
  {{- end }}
{{- end }}
{{- range .Values.extraConfigMap }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-config-{{ .name }}
  configMap:
    name: {{ .name }}
  {{- end }}
{{- end }}
{{- if .Values.secret }}
  {{- if .Values.secret.externalSecrets }}
    {{- range .Values.secret.externalSecrets }}
      {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-secret-{{ .name }}
  secret:
    secretName: {{ .name }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .Values.extraSecret }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-secret-{{ .name }}
  secret:
    secretName: {{ .name }}
  {{- end }}
{{- end }}
{{- with .Values.volumes }}
{{- toYaml . | nindent 8 }}
{{- end }}
{{ end }}