{{ define "volumeMount" }}
{{- if or .Values.volumeMounts .Values.persistence.enabled .Values.secret }}
{{- if .Values.persistence.enabled }}
  {{- range .Values.persistence.pvcs }}
- name: vol-{{ .name }}
  mountPath: {{ .mountPath }}
  {{- end }}
{{- end }}
{{- range .Values.configmap }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-config-{{ .name }}
  mountPath: {{ .mountPath }}
  {{- if .subPath }}
  subPath: {{ .subPath }}
  {{- end }}
  readOnly: {{ .readOnly | default true }}
  {{- end }}
{{- end }}
{{- range .Values.extraConfigMap }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-config-{{ .name }}
  mountPath: {{ .mountPath }}
  {{- if .subPath }}
  subPath: {{ .subPath }}
  {{- end }}
  readOnly: {{ .readOnly | default true }}
  {{- end }}
{{- end }}
{{- if .Values.secret.externalSecrets }}
  {{- range .Values.secret.externalSecrets }}
    {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-secret-{{ .name }}
  mountPath: {{ .mountPath }}
  {{- if .subPath }}
  subPath: {{ .subPath }}
  {{- end }}
  readOnly: {{ .readOnly | default true }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .Values.extraSecret }}
  {{- if (eq (hasKey . "mountPath") true) }}
- name: vol-secret-{{ .name }}
  mountPath: {{ .mountPath }}
  {{- if .subPath }}
  subPath: {{ .subPath }}
  {{- end }}
  readOnly: {{ .readOnly | default true }}
  {{- end }}
{{- end }}
{{- with .Values.volumeMounts }}
{{- toYaml . | nindent 12 }}
{{- end }}
{{- end }}
{{ end }}