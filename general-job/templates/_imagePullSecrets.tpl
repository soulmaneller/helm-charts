{{ define "imagePullSecrets" }}
{{- range .Values.imagePullSecrets }}
  - name: {{ .name }}
{{- end }}
{{ end }}