{{ define "envFrom" }}
{{- range $configmaps := .Values.configmap }}
  {{- with (coalesce $configmaps.key) }}
    {{- if (eq (hasKey $configmaps "mountPath") false) }}
- configMapRef:
    name: {{ tpl $configmaps.name $ }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .Values.extraConfigMap }}
  {{- if (eq (hasKey . "mountPath") false) }}
- configMapRef:
    name: {{ .name }}
  {{- end}}
{{- end}}
{{- if .Values.secret }}
  {{- if .Values.secret.externalSecrets }}
    {{- range .Values.secret.externalSecrets }}
      {{- if (eq (hasKey . "mountPath") false) }}
- secretRef:
    name: {{ .name }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .Values.extraSecret }}
  {{- if (eq (hasKey . "mountPath") false) }}
- secretRef:
    name: {{ .name }}
  {{- end}}
{{- end}}
{{ end }}