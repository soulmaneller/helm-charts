## Installing

### Prerequisites

With the command helm version, make sure that you have:

- Helm v3 installed

Add Traefik's chart repository to Helm:

`helm repo add helm-charts https://gitlab.com/api/v4/projects/57542704/packages/helm/stable`

You can update the chart repository by running:

`helm repo update`

Deploying Traefik

`helm install general helm-charts/general`
