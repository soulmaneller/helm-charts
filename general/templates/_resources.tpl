{{ define "resources" }}
requests:
  cpu: {{ .requests.cpu }}
  memory: {{ .requests.memory }}
  ephemeral-storage: {{ index  .requests "ephemeral-storage" }}
limits:
  cpu: {{ .limits.cpu | default (include "general.mulWithUnit" (list .requests.cpu 1.5)) }}
  memory: {{ .limits.memory | default (include "general.mulWithUnit" (list .requests.memory 1.5)) }}
  ephemeral-storage: {{ (index .limits "ephemeral-storage") | default (include "general.mulWithUnit" (list (index .requests "ephemeral-storage") 1.5)) }}
{{ end }}