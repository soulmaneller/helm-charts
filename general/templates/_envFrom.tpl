{{ define "envFrom" }}
{{- range $configmaps := .configmap }}
  {{- with (coalesce $configmaps.key) }}
    {{- if (eq (hasKey $configmaps "mountPath") false) }}
- configMapRef:
    name: {{ tpl $configmaps.name $ }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .extraConfigMap }}
  {{- if (eq (hasKey . "mountPath") false) }}
- configMapRef:
    name: {{ .name }}
  {{- end}}
{{- end}}
{{- if .secret }}
  {{- if .secret.externalSecrets }}
    {{- range .secret.externalSecrets }}
      {{- if (eq (hasKey . "mountPath") false) }}
- secretRef:
    name: {{ .name }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{{- range .extraSecret }}
  {{- if (eq (hasKey . "mountPath") false) }}
- secretRef:
    name: {{ .name }}
  {{- end}}
{{- end}}
{{ end }}